package com.kopismobile.manta01;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.widget.RelativeLayout;

/**
 * Created by jderienzo on 8/20/2016.
 */
public class GroupItemView extends RelativeLayout {

    private AppCompatActivity activity      = null;
    private MantaListAdapter  listAdapter   = null;

    public GroupItemView(Context context) {
        super(context);
    }

    public GroupItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GroupItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setActivity(AppCompatActivity a) {
        this.activity = a;
        return;
    }

    public void setListAdapter(MantaListAdapter a) {
        this.listAdapter = a;
        return;
    }

    @Override
    protected void onCreateContextMenu(ContextMenu menu) {
        if (this.getId() == R.id.listItemGroup) {
            this.activity.getMenuInflater().inflate(R.menu.menu_group_item, menu);
            int groupPosition = (int) this.getTag(R.id.listItemGroup);
            String groupName = (String)this.getTag(R.id.groupName);
            Cursor c = this.listAdapter.getActivity().getContentResolver().query(
                    MantaContentProvider.MANTA_URI,
                    new String[]{MantaTable.ID},
                    MantaTable.GROUP_NAME + " = '" + groupName + "'",
                    null,
                    null
            );
            MenuItem moveItem = menu.findItem(R.id.addAllToGroup);
            MenuItem renameItem = menu.findItem(R.id.renameGroup);
            if((c != null) && (c.getCount() > 0)) {
                moveItem.setEnabled(true);
            }
            else{
                moveItem.setEnabled(false);
            }
            renameItem.setEnabled(groupPosition != 0);
            if(c != null){
                c.close();
            }
            Intent i = new Intent();
            i.putExtra("sourceGroup", groupName);
            moveItem.setIntent(i);
            renameItem.setIntent(i);
        }
        super.onCreateContextMenu(menu);
        return;
    }
}