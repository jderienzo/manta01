package com.kopismobile.manta01;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by jderienzo on 8/17/2016.
 */
public class GroupTable {
    private static final String   TAG = GroupTable.class.getSimpleName();

    // Database table
    public static final String TABLE_NAME         = "GroupTable";
    public static final String ID                 = "_id";
    public static final String NAME               = "Name";

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_NAME
            + "("
            + ID + " integer primary key autoincrement, "
            + NAME + " text"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        Log.v(TAG, "onCreate(): " + DATABASE_CREATE);
        database.execSQL(DATABASE_CREATE);
        return;
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
        return;
    }
}
