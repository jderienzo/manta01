package com.kopismobile.manta01;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

public class MantaContentProvider extends ContentProvider {

    private static final String AUTHORITY     = "com.kopismobile.manta01.contentprovider";

    private static final int    MANTA          = 11;
    private static final int    MANTA_ID       = 21;
    private static final String MANTA_BASE     = "manta";
    private static final int    GROUP          = 12;
    private static final int    GROUP_ID       = 22;
    private static final String GROUP_BASE     = "group";
    private static final int    MESSAGE        = 13;
    private static final int    MESSAGE_ID     = 23;
    private static final String MESSAGE_BASE   = "message";

    public  static final Uri    MANTA_URI      = Uri.parse("content://" + AUTHORITY + "/" + MANTA_BASE);
    public  static final Uri    GROUP_URI      = Uri.parse("content://" + AUTHORITY + "/" + GROUP_BASE);
    public  static final Uri    MESSAGE_URI    = Uri.parse("content://" + AUTHORITY + "/" + MESSAGE_BASE);

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        uriMatcher.addURI(AUTHORITY, MANTA_BASE, MANTA);
        uriMatcher.addURI(AUTHORITY, MANTA_BASE + "/#", MANTA_ID);
        uriMatcher.addURI(AUTHORITY, GROUP_BASE, GROUP);
        uriMatcher.addURI(AUTHORITY, GROUP_BASE + "/#", GROUP_ID);
        uriMatcher.addURI(AUTHORITY, MESSAGE_BASE, MESSAGE);
        uriMatcher.addURI(AUTHORITY, MESSAGE_BASE + "/#", MESSAGE_ID);
    }

    // database
    private MantaDatabaseHelper database = null;

    @Override
    public boolean onCreate() {
        this.database = new MantaDatabaseHelper(getContext());
        return(false);
    }

    @Override
    synchronized public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = uriMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsDeleted = 0;
        String id = uri.getLastPathSegment();
        switch(uriType) {
            case MANTA:
                rowsDeleted = sqlDB.delete(MantaTable.TABLE_NAME, selection, selectionArgs);
                break;
            case MANTA_ID:
                if(TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(MantaTable.TABLE_NAME, MantaTable.ID + "=" + id, null);
                }
                else {
                    rowsDeleted = sqlDB.delete(MantaTable.TABLE_NAME, MantaTable.ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case GROUP:
                rowsDeleted = sqlDB.delete(GroupTable.TABLE_NAME, selection, selectionArgs);
                break;
            case GROUP_ID:
                if(TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(GroupTable.TABLE_NAME, GroupTable.ID + "=" + id, null);
                }
                else {
                    rowsDeleted = sqlDB.delete(GroupTable.TABLE_NAME, GroupTable.ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case MESSAGE:
                rowsDeleted = sqlDB.delete(MessageTable.TABLE_NAME, selection, selectionArgs);
                break;
            case MESSAGE_ID:
                if(TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(MessageTable.TABLE_NAME, MessageTable.ID + "=" + id, null);
                }
                else {
                    rowsDeleted = sqlDB.delete(MessageTable.TABLE_NAME, MessageTable.ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        ContentResolver cr;
        if((cr = getContext().getContentResolver()) != null){
            cr.notifyChange(uri, null);
        }
        return(rowsDeleted);
    }

    @Override
    synchronized public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        // Using SQLiteQueryBuilder instead of query() method
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        // check uri
        int uriType = uriMatcher.match(uri);
        switch(uriType) {
            case MANTA:
                queryBuilder.setTables(MantaTable.TABLE_NAME);
                break;
            case MANTA_ID:
                queryBuilder.setTables(MantaTable.TABLE_NAME);
                // Adding the ID to the original query
                queryBuilder.appendWhere(MantaTable.ID + "=" + uri.getLastPathSegment());
                break;
            case GROUP:
                queryBuilder.setTables(GroupTable.TABLE_NAME);
                break;
            case GROUP_ID:
                queryBuilder.setTables(GroupTable.TABLE_NAME);
                // Adding the ID to the original query
                queryBuilder.appendWhere(GroupTable.ID + "=" + uri.getLastPathSegment());
                break;
            case MESSAGE:
                queryBuilder.setTables(MessageTable.TABLE_NAME);
                break;
            case MESSAGE_ID:
                queryBuilder.setTables(MessageTable.TABLE_NAME);
                // Adding the ID to the original query
                queryBuilder.appendWhere(MessageTable.ID + "=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        // query
        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        // Make sure that potential listeners are getting notified
        ContentResolver cr;
        if((cr = getContext().getContentResolver()) != null) {
            cursor.setNotificationUri(cr, uri);
        }
        return(cursor);
    }

    @Override
    public String getType(Uri arg0) {
        return(null);
    }

    @Override
    synchronized public Uri insert(Uri uri, ContentValues values) {
        int uriType = MantaContentProvider.uriMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        long id = 0;
        String basePath = "";
        switch(uriType) {
            case MANTA:
                id = sqlDB.insert(MantaTable.TABLE_NAME, null, values);
                basePath = MANTA_BASE;
                break;
            case GROUP:
                id = sqlDB.insert(GroupTable.TABLE_NAME, null, values);
                basePath = GROUP_BASE;
                break;
            case MESSAGE:
                id = sqlDB.insert(MessageTable.TABLE_NAME, null, values);
                basePath = MESSAGE_BASE;
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        ContentResolver cr;
        if((cr = getContext().getContentResolver()) != null) {
            cr.notifyChange(uri, null);
        }
        return(Uri.parse(basePath + "/" + id));
    }

    @Override
    synchronized public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int uriType = MantaContentProvider.uriMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsUpdated = 0;
        String id = null;
        switch(uriType) {
            case MANTA:
                rowsUpdated = sqlDB.update(MantaTable.TABLE_NAME, values, selection, selectionArgs);
                break;
            case MANTA_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(MantaTable.TABLE_NAME, values, MantaTable.ID + "=" + id, null);
                } else {
                    rowsUpdated = sqlDB.update(MantaTable.TABLE_NAME, values, MantaTable.ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case GROUP:
                rowsUpdated = sqlDB.update(GroupTable.TABLE_NAME, values, selection, selectionArgs);
                break;
            case GROUP_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(GroupTable.TABLE_NAME, values, GroupTable.ID + "=" + id, null);
                } else {
                    rowsUpdated = sqlDB.update(GroupTable.TABLE_NAME, values, GroupTable.ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            case MESSAGE:
                rowsUpdated = sqlDB.update(MessageTable.TABLE_NAME, values, selection, selectionArgs);
                break;
            case MESSAGE_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(MessageTable.TABLE_NAME, values, MessageTable.ID + "=" + id, null);
                } else {
                    rowsUpdated = sqlDB.update(MessageTable.TABLE_NAME, values, MessageTable.ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        ContentResolver cr;
        if((cr = getContext().getContentResolver()) != null) {
            cr.notifyChange(uri, null);
        }
        return(rowsUpdated);
    }

}
