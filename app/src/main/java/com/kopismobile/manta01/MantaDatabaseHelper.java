package com.kopismobile.manta01;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MantaDatabaseHelper extends SQLiteOpenHelper {
    private static final String   TAG = MantaDatabaseHelper.class.getSimpleName();

    private static final String DATABASE_NAME    = "manta.db";
    private static final int    DATABASE_VERSION = 20;

    public MantaDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        MantaTable.onCreate(db);
        GroupTable.onCreate(db);
        MessageTable.onCreate(db);
        return;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        MantaTable.onUpgrade(db, oldVersion, newVersion);
        GroupTable.onUpgrade(db, oldVersion, newVersion);
        MessageTable.onUpgrade(db, oldVersion, newVersion);
        return;
    }
}
