package com.kopismobile.manta01;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

/**
 * Created by jderienzo on 8/17/2016.
 */
public class MantaDb {
    private static final String TAG = MantaDb.class.getSimpleName();

    public static void dumpTables(Context context){
        MantaDb.dumpTable(context, MantaContentProvider.GROUP_URI);
        MantaDb.dumpTable(context, MantaContentProvider.MANTA_URI);
        MantaDb.dumpTable(context, MantaContentProvider.MESSAGE_URI);
        return;
    }

    public static void dumpTable(Context context, Uri uri){
        Log.i(TAG, "dumpTable(" + uri.toString() + ")");
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        if(cursor != null){
            if(cursor.moveToFirst()){
                Log.i(TAG, "record:");
                while(!cursor.isAfterLast()){
                    MantaDb.dumpCursorRow(cursor);
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
        return;
    }

    public static void dumpCursorRow(Cursor cursor){
        String[] colNames = cursor.getColumnNames();
        for(int i = 0; i < colNames.length; i++){
            String value = cursor.getString(cursor.getColumnIndex(colNames[i]));
            Log.i(TAG, "  " + colNames[i] + "=" + value);
        }
        return;
    }

    public static void setNodeNewById(Context context, int id, boolean isNew){
        ContentValues cv = new ContentValues();
        cv.put(MantaTable.IS_NEW, isNew?1:0);
        context.getContentResolver().update(MantaContentProvider.MANTA_URI, cv, MantaTable.ID + " = " + id, null);
        return;
    }

    public static void clearMessages(Context context){
        context.getContentResolver().delete(MantaContentProvider.MESSAGE_URI, null, null);
        return;
    }

    public static void deleteMessage(Context context, int id){
        context.getContentResolver().delete(MantaContentProvider.MESSAGE_URI, MessageTable.ID + " = " + id, null);
        return;
    }

    public static void sendMessage(Context context, String from, String to, int req, String data){
        ContentValues cv = new ContentValues();
        cv.put(MessageTable.FROM, from);
        cv.put(MessageTable.TO, to);
        cv.put(MessageTable.REQUEST_ID, req);
        cv.put(MessageTable.DATA, data);
        context.getContentResolver().insert(MantaContentProvider.MESSAGE_URI, cv);
        return;
    }

    public static void moveGroupToGroup(Context context, String fromGroup, String toGroup){
        // make a new group if destination doesn't exist
        Cursor cursor = context.getContentResolver().query(
                MantaContentProvider.GROUP_URI,
                new String[]{GroupTable.ID},
                GroupTable.NAME + " = '" + toGroup + "'",
                null,
                null
        );
        if((cursor == null) || (cursor.getCount() <= 0)){
            ContentValues cv = new ContentValues();
            cv.put(GroupTable.NAME, toGroup);
            context.getContentResolver().insert(
                    MantaContentProvider.GROUP_URI,
                    cv
            );
        }
        if(cursor != null){
            cursor.close();
        }
        // move the source group into the destination group
        ContentValues cv = new ContentValues();
        cv.put(MantaTable.GROUP_NAME, toGroup);
        cv.put(MantaTable.IS_NEW, 1);
        context.getContentResolver().update(
                MantaContentProvider.MANTA_URI,
                cv,
                MantaTable.GROUP_NAME + " = '" + fromGroup + "'",
                null
        );
        return;
    }
}
