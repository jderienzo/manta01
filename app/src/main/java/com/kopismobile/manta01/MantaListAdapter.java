package com.kopismobile.manta01;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorTreeAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jderienzo on 8/15/2016.
 */
public class MantaListAdapter extends CursorTreeAdapter implements LoaderManager.LoaderCallbacks<Cursor>, AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener, View.OnClickListener {

    private static final String   TAG            = MantaListAdapter.class.getSimpleName();

    final static private int GROUP_LOADER_ID = 1;
    final static private int CHILD_LOADER_ID = 2;

    private class GroupItemState {
        public View rootView;
        public TextView nameView;
        public String name;
        public String uid;
    }

    private class ChildItemState {
        public View rootView;
        public TextView nameView;
        public TextView idView;
        public String name;
        public String uid;
    }

    private AppCompatActivity activity      = null;
    private String[]          groupProjection    = new String[]{
            GroupTable.ID,
            GroupTable.NAME,
    };
    private String[]          childProjection    = new String[]{
            MantaTable.ID,
            MantaTable.IS_NEW,
            MantaTable.NAME,
            MantaTable.UID,
            MantaTable.GROUP_NAME
    };
    private String            selection     = null;
    private String            sort          = null;
    private LoaderManager     loaderManager = null;
    private ExpandableListView listView = null;
    private Map<Integer, String> groupMap   = null;

    public MantaListAdapter(AppCompatActivity activity, Cursor groupCursor) {
        super(groupCursor, activity, true);
        this.activity = activity;

        this.groupMap = new HashMap<Integer, String>();
        this.loaderManager = activity.getSupportLoaderManager();
        this.loaderManager.initLoader(GROUP_LOADER_ID, null, this);

        this.loaderManager = activity.getSupportLoaderManager();
        this.loaderManager.initLoader(GROUP_LOADER_ID, null, this);

        this.listView = (ExpandableListView) (activity.findViewById(R.id.mainList));
    }

    @Override
    protected Cursor getChildrenCursor(Cursor groupCursor) {
        // Given the group, we return a cursor for all the children within that group
        final String groupName = groupCursor.getString(groupCursor.getColumnIndex(GroupTable.NAME));
        final CursorLoader cursorLoader = new CursorLoader(
                this.activity,
                MantaContentProvider.MANTA_URI,
                null,
                MantaTable.GROUP_NAME + " = '" + groupName + "'",
                null,
                null);
        Cursor childCursor = null;
        try {
            childCursor = cursorLoader.loadInBackground();
            childCursor.moveToFirst();
        } catch (final Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return(childCursor);
    }

    @Override
    public Cursor getChild(int groupPosition, int childPosition) {
        return super.getChild(groupPosition, childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return super.getChildId(groupPosition, childPosition);
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return super.getChildrenCount(groupPosition);
    }

    @Override
    public int getGroupCount() {
        int c = super.getGroupCount();
        Log.v(TAG, "getGroupCount(): nGroups=" + c);
        return(c);
    }

    @Override
    public long getGroupId(int groupPosition) {
        long groupId = super.getGroupId(groupPosition);
        Log.v(TAG, "getGroupId(): groupPosition=" + groupPosition + ", groupId=" + groupId);
        return(groupId);
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View view, ViewGroup viewGroup) {
        Log.v(TAG, "getGroupView(): groupPosition=" + groupPosition);
        GroupItemState itemState = null;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.group_list_item, null);
            ((GroupItemView)view).setActivity(this.activity);
            ((GroupItemView)view).setListAdapter(this);
            itemState = new GroupItemState();
            view.setTag(itemState);
            itemState.rootView = view;
            itemState.nameView = (TextView) view.findViewById(R.id.groupName);
            view.setLongClickable(true);
            view.setOnClickListener(this);
        }
        view.setTag(R.id.listItemGroup, groupPosition);
        view.setTag(R.id.groupName, this.groupMap.get((int)(long)this.getGroupId(groupPosition)));
        itemState = (GroupItemState) view.getTag();
        final GroupItemState finalItemState = itemState;
        this.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                finalItemState.nameView.setText(MantaListAdapter.this.groupMap.get((int)(long)MantaListAdapter.this.getGroupId(groupPosition)));
                return;
            }
        });
        return (view);
    }

    @Override
    protected View newGroupView(Context context, Cursor cursor, boolean b, ViewGroup viewGroup) {
        return null;
    }

    @Override
    protected void bindGroupView(View view, Context context, Cursor cursor, boolean b) {
        return;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean b, View view, ViewGroup viewGroup) {
        Log.v(TAG, "getChildView(): groupPosition=" + groupPosition + ", childPosition=" + childPosition);
        ChildItemState itemState = null;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.node_list_item, null);
            itemState = new ChildItemState();
            itemState.rootView = view;
            itemState.nameView = (TextView) view.findViewById(R.id.nodeName);
            itemState.idView = (TextView) view.findViewById(R.id.nodeID);
            view.setTag(itemState);
            view.setLongClickable( true);
        }
        itemState = (ChildItemState) view.getTag();
        Cursor c = this.getChildrenCursor(this.getCursor());
        if(c.move(childPosition)) {
            MantaDb.dumpCursorRow(c);
            final String name = c.getString(c.getColumnIndex(MantaTable.NAME));
            final int id = c.getInt(c.getColumnIndex(MantaTable.ID));
            final String uid = c.getString(c.getColumnIndex(MantaTable.UID));
            final ChildItemState finalItemState = itemState;
            final boolean isNew = c.getInt(c.getColumnIndex(MantaTable.IS_NEW)) != 0;
            final View finalView = view;
            this.activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    finalItemState.nameView.setText(name);
                    finalItemState.idView.setText(uid);
                    if(isNew){
                        finalView.setAlpha(0.0f);
                        finalView.setScaleX(0.0f);
                        finalView.animate().scaleX(1.0f).alpha(1.0f).setDuration(300).start();
                        MantaDb.setNodeNewById(MantaListAdapter.this.activity, id, false);
                    }
                    return;
                }
            });
        }
        c.close();
        return (view);
    }

    @Override
    protected View newChildView(Context context, Cursor cursor, boolean b, ViewGroup viewGroup) {
        return null;
    }

    @Override
    protected void bindChildView(View view, Context context, Cursor cursor, boolean b) {
        return;
    }

    @Override
    public void onClick(View view) {
        Log.v(TAG, "onClick(): view=" + view);
        Integer gp = (Integer) view.getTag(R.id.listItemGroup);
        if(gp != null){
            if(this.listView.isGroupExpanded(gp)){
                this.listView.collapseGroup(gp);
            }
            else{
                this.listView.expandGroup(gp, true);
            }
        }
        return;
    }

    public AppCompatActivity getActivity(){
        return(this.activity);
    }

    /*********************************************************
     *        LoaderManager.LoaderCallbacks interface
     *********************************************************/
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Loader<Cursor> result = null;
        switch (id) {
            case GROUP_LOADER_ID:
                // Returns a new CursorLoader
                result = new CursorLoader(
                        // Parent activity context
                        this.activity,
                        // Table to query
                        MantaContentProvider.GROUP_URI,
                        // Projection
                        this.groupProjection,
                        // Selection
                        this.selection,
                        // Selection arguments
                        null,
                        // Sort order
                        this.sort
                );
                break;
            case CHILD_LOADER_ID:
                // Returns a new CursorLoader
                result = new CursorLoader(
                        // Parent activity context
                        this.activity,
                        // Table to query
                        MantaContentProvider.MANTA_URI,
                        // Projection
                        this.childProjection,
                        // Selection
                        this.selection,
                        // Selection arguments
                        null,
                        // Sort order
                        this.sort
                );
                break;
            default:
                // An invalid id was passed in
        }
        return(result);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        switch (loader.getId()) {
            case GROUP_LOADER_ID:
                int i = cursor.getCount();
                this.setGroupCursor(cursor);
                cursor.moveToFirst();
                while(!cursor.isAfterLast()){
                    int j = cursor.getInt(cursor.getColumnIndex(GroupTable.ID));
                    this.groupMap.put(
                            cursor.getInt(cursor.getColumnIndex(GroupTable.ID)),
                            cursor.getString(cursor.getColumnIndex(GroupTable.NAME))
                    );
                    cursor.moveToNext();
                }
                this.activity.invalidateOptionsMenu();
                break;
            case CHILD_LOADER_ID:
                int j = cursor.getCount();
                this.setChildrenCursor(loader.getId(), cursor);
                this.activity.invalidateOptionsMenu();
                break;
        }
        return;
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        return;
    }



   /*********************************************************
    *    AdapterView.OnItemLongClickListener interface
    *********************************************************/

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        return true;
    }

    /*********************************************************
     *       AdapterView.OnItemClickListener interface
     *********************************************************/

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Log.v(TAG, "onItemClick(): view=" + view);
        return;
    }
}
