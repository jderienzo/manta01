package com.kopismobile.manta01;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;

/**
 * Created by jderienzo on 8/22/2016.
 */
public class MantaService extends Service implements Loader.OnLoadCompleteListener<Cursor> {
    private static final String TAG = MantaService.class.getSimpleName();

    private static final int MESSAGE_LOADER_ID = 20;

    private Handler          bgHandler        = null;
    private BluetoothAdapter bluetoothAdapter = null;
    private CursorLoader     cursorLoader     = null;
    private NodeScanner      nodeScanner      = null;

    public MantaService(){
        super();
    }

    @Override
    public void onCreate(){
        super.onCreate();
//		android.os.Debug.waitForDebugger();

        HandlerThread ht = new HandlerThread("BackgroundThread");
        ht.start();
        this.bgHandler = new Handler(ht.getLooper());

        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        this.bluetoothAdapter = bluetoothManager.getAdapter();

        this.cursorLoader = new CursorLoader(
                // activity context
                this,
                // Table to query
                MantaContentProvider.MESSAGE_URI,
                // Projection
                new String[]{MessageTable.ID, MessageTable.REQUEST_ID, MessageTable.TIME, MessageTable.DATA},
                // Selection
                MessageTable.TO + " = '" + MessageTable.FROM_TO_SERVICE + "'",
                // Selection arguments
                null,
                // Sort order
                MessageTable.TIME
        );
        this.cursorLoader.registerListener(MESSAGE_LOADER_ID, this);
        this.cursorLoader.startLoading();

        return;
    }

    @Override
    public void onDestroy() {
        Log.v(TAG, "onDestroy()");
        if(this.nodeScanner != null){
            this.nodeScanner.finish();
        }
        if(this.bgHandler != null){
            this.bgHandler.postAtFrontOfQueue(new Runnable() {
                @Override
                public void run() {
                    Looper.myLooper().quit();
                }
            });
        }
        if (this.cursorLoader != null) {
            this.cursorLoader.unregisterListener(this);
            this.cursorLoader.cancelLoad();
            this.cursorLoader.stopLoading();
        }
        super.onDestroy();
        return;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return(null);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Log.w(TAG, "onStartCommand(): startId=" + startId + ", intent=" + intent);
        if (this.bluetoothAdapter.isEnabled()) {
            if(this.nodeScanner == null){
                this.nodeScanner = NodeScanner.createNodeScanner(this, null);
                this.nodeScanner.start();
            }
        }
        return(Service.START_NOT_STICKY);
    }

    /*********************************************************
     *       OnLoadCompleteListener interface
     *********************************************************/

    @Override
    public void onLoadComplete(Loader<Cursor> loader, Cursor cursor) {
        switch (loader.getId()) {
            case MESSAGE_LOADER_ID:
                cursor.moveToFirst();
                while(!cursor.isAfterLast()) {
                    int request = cursor.getInt(cursor.getColumnIndex(MessageTable.REQUEST_ID));
                    int id = cursor.getInt(cursor.getColumnIndex(MessageTable.ID));
                    // TODO: should handle commands in a handler
                    switch (request) {
                        case MessageTable.REQ_START_SCAN:
                            MantaDb.deleteMessage(this, id);
                            break;
                    }
                    cursor.moveToNext();
                }
                break;
        }
        return;
    }

}
