package com.kopismobile.manta01;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by jderienzo on 2/15/2016.
 */
public class MantaTable {
    private static final String   TAG = MantaTable.class.getSimpleName();

    // Database table
    public static final String TABLE_NAME         = "MantaTable";
    public static final String ID                 = "_id";
    public static final String IS_NEW             = "IsNew";
    public static final String IS_STALE           = "IsStale";
    public static final String NAME               = "Name";
    public static final String GROUP_NAME         = "GroupName";
    public static final String UID                = "Uid";
    public static final String DISCOVERY_TIME     = "DiscoveryTime";

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_NAME
            + "("
            + ID + " integer primary key autoincrement, "
            + IS_NEW + " integer, "
            + IS_STALE + " integer, "
            + DISCOVERY_TIME + " integer, "
            + NAME + " text, "
            + GROUP_NAME + " text, "
            + UID + " text not null"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        Log.v(TAG, "onCreate(): " + DATABASE_CREATE);
        database.execSQL(DATABASE_CREATE);
        return;
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
        return;
    }
}
