package com.kopismobile.manta01;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by jderienzo on 8/23/2016.
 */
public class MessageTable {
    private static final String   TAG = MessageTable.class.getSimpleName();

    // strings for FROM and TO fields
    public static final String  FROM_TO_SERVICE = "SVC";
    public static final String  FROM_TO_UI = "UI";

    // requests from activity to service
    public static final int REQ_START_SCAN = 1;
    // requests from service to activity
    public static final int REQ_ASK_BLE_ON = 2;

    // Database table
    public static final String TABLE_NAME         = "MessageTable";
    public static final String ID                 = "_id";
    public static final String FROM               = "MsgFrom";
    public static final String TO                 = "MsgTo";
    public static final String REQUEST_ID         = "RequestId";
    public static final String TIME               = "Time";
    public static final String DATA               = "Data";

    // Database creation SQL statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_NAME
            + "("
            + ID + " integer primary key autoincrement, "
            + FROM + " text not null, "
            + TO + " text not null, "
            + TIME + " integer, "
            + REQUEST_ID + " integer, "
            + DATA + " text"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        Log.v(TAG, "onCreate(): " + DATABASE_CREATE);
        database.execSQL(DATABASE_CREATE);
        return;
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
        return;
    }
}
