package com.kopismobile.manta01;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by jderienzo on 8/25/2016.
 */
public class NodeItemView extends RelativeLayout {

    public NodeItemView(Context context) {
        super(context);
    }

    public NodeItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NodeItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
