package com.kopismobile.manta01;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

public class NodeManagerActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private static final String TAG                 = NodeManagerActivity.class.getSimpleName();
    private static final int    REQUEST_ENABLE_BT   = 1;
    private static final int    MESSAGE_LOADER_ID   = 10;

    private ExpandableListView listView               = null;
    private MantaListAdapter   listAdapter            = null;
    private boolean            isBluetoothLeSupported = false;
    private BluetoothAdapter   bluetoothAdapter       = null;
    private LoaderManager      loaderManager          = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_node_manager);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        MantaDb.clearMessages(this);

        // test... /////////////////////////////////////////////////////////
        /*
        String[] groups = {
                "<ungrouped>",
                "Group A",
                "Group B",
                "Group C"
        };
        Random rand = new Random(System.currentTimeMillis());
        ContentValues cv = new ContentValues();
        for(int i=0; i<groups.length; i++){
            cv.put(MantaTable.NAME, groups[i]);
            this.getContentResolver().insert(MantaContentProvider.GROUP_URI, cv);
        }
        cv.clear();
        for(int i=0; i<10; i++){
            int r = rand.nextInt(groups.length);
            cv.put(MantaTable.GROUP_NAME, groups[r]);
            cv.put(MantaTable.NAME, "MantaNode-" + i);
            cv.put(MantaTable.IS_NEW, 1);
            cv.put(MantaTable.UID, String.valueOf(System.currentTimeMillis()));
            this.getContentResolver().insert(MantaContentProvider.MANTA_URI, cv);
        }

        ContentValues cv2 = new ContentValues();
        for(int i=0; i<10; i++){
            cv2.put(MantaTable.IS_NEW, 1);
            this.getContentResolver().update(MantaContentProvider.MANTA_URI, cv2, null, null);
        }
        */
        MantaDb.dumpTables(this);
        //////////////////////////////////////////////////////////////////

        // build the list view
        this.listView = (ExpandableListView) this.findViewById(R.id.mainList);
        final CursorLoader cursorLoader = new CursorLoader(
                this,
                MantaContentProvider.GROUP_URI,
                null,
                null,
                null,
                null);
        Cursor groupsCursor = null;
        try {
            groupsCursor = cursorLoader.loadInBackground();
            groupsCursor.moveToFirst();
        } catch (final Exception e) {
            Log.e(TAG, e.getMessage());
        }

        this.listAdapter = new MantaListAdapter(this, groupsCursor);
        if((this.listView != null) && (this.listAdapter != null)){
            listView.setAdapter(this.listAdapter);
        }

        this.isBluetoothLeSupported = this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
        final BluetoothManager btManager = (BluetoothManager) this.getSystemService(Context.BLUETOOTH_SERVICE);
        this.bluetoothAdapter = btManager.getAdapter();
        if((this.isBluetoothLeSupported) && (this.bluetoothAdapter != null)){
            ComponentName cn = this.startService(new Intent(this, MantaService.class));
            Log.v(TAG, "onCreate(): componentName=" + cn);
        }
        else{
            // !!! big problem...no BLE
            Toast.makeText(this, "BLE Not Supported", Toast.LENGTH_SHORT).show();
            finish();
        }

        this.loaderManager = this.getSupportLoaderManager();
        this.loaderManager.initLoader(MESSAGE_LOADER_ID, null, this);

        this.initDb();
        return;
    }

    @Override
    protected void onDestroy() {
        this.stopService(new Intent(this, MantaService.class));
        super.onDestroy();
        return;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (this.bluetoothAdapter == null || !this.bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        return;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        boolean result = true;
        switch(item.getItemId()){
            case R.id.addAllToGroup:
                final String sourceGroup1 = item.getIntent().getStringExtra("sourceGroup");
                Log.v(TAG, "onOptionsItemSelected(): Add all to group:  source=" + sourceGroup1);
                (new ToGroupDialog(this, sourceGroup1, new ToGroupDialog.ToGroupCallback() {
                    @Override
                    public void onGroupName(String toGroup) {
                        if(toGroup != null) {
                            MantaDb.moveGroupToGroup(NodeManagerActivity.this, sourceGroup1, toGroup);
                        }
                        return;
                    }
                })).show();
                break;
            case R.id.renameGroup:
                final String sourceGroup2 = item.getIntent().getStringExtra("sourceGroup");
                Log.v(TAG, "onOptionsItemSelected(): Rename group:  source=" + sourceGroup2);
                break;
            default:
                result = false;
        }
        return(result);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        int i = v.getId();
        if (v.getId() == R.id.listItemGroup) {
            this.getMenuInflater().inflate(R.menu.menu_group_item, menu);
        }
        super.onCreateContextMenu(menu, v, menuInfo);
        return;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_CANCELED) {
                //Bluetooth not enabled.
                finish();
                return;
            }
            ComponentName cn = this.startService(new Intent(this, MantaService.class));
            Log.v(TAG, "onActivityResult(): componentName=" + cn);
            MantaDb.sendMessage(this, MessageTable.FROM_TO_UI, MessageTable.FROM_TO_SERVICE, MessageTable.REQ_START_SCAN, null);
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
        return;
    }

    private void checkBleOn() {
        if (!bluetoothAdapter.isEnabled()) {
            final Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            this.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        return;
    }

    private void initDb(){
        Cursor c = this.getContentResolver().query(MantaContentProvider.GROUP_URI, new String[]{GroupTable.ID}, GroupTable.NAME + " = '<unknown>'", null, null);
        if((c == null) || (c.getCount() <= 0)) {
            ContentValues cv = new ContentValues();
            cv.put(MantaTable.NAME, "<unknown>");
            this.getContentResolver().insert(MantaContentProvider.GROUP_URI, cv);
        }
        if(c != null){
            c.close();
        }
        return;
    }

    /*********************************************************
     *       LoaderManager.LoaderCallbacks interface
     *********************************************************/

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Loader<Cursor> result = null;
        switch (id) {
            case MESSAGE_LOADER_ID:
                // return a new CursorLoader
                result = new CursorLoader(
                        // activity context
                        this,
                        // Table to query
                        MantaContentProvider.MESSAGE_URI,
                        // Projection
                        new String[]{MessageTable.ID, MessageTable.REQUEST_ID, MessageTable.TIME, MessageTable.DATA},
                        // Selection
                        MessageTable.TO + " = '" + MessageTable.FROM_TO_UI + "'",
                        // Selection arguments
                        null,
                        // Sort order
                        MessageTable.TIME
                );
                break;
            default:
                // An invalid id came in
        }
        return(result);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        switch (loader.getId()) {
            case MESSAGE_LOADER_ID:
                cursor.moveToFirst();
                while(!cursor.isAfterLast()) {
                    int request = cursor.getInt(cursor.getColumnIndex(MessageTable.REQUEST_ID));
                    int id = cursor.getInt(cursor.getColumnIndex(MessageTable.ID));
                    // TODO: should handle commands in a handler
                    switch (request) {
                        case MessageTable.REQ_ASK_BLE_ON:
                            MantaDb.deleteMessage(this, id);
                            this.checkBleOn();
                            break;
                    }
                    cursor.moveToNext();
                }
                break;
        }
        return;
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
