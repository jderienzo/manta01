package com.kopismobile.manta01;

import android.content.Context;
import android.os.Build;

/**
 * Created by jderienzo on 8/28/2016.
 */
abstract public class NodeScanner {

    public interface NodeScannerCallbacks {
        void finished(boolean success);
        void foundDevice(String device);
    }

    static public NodeScanner createNodeScanner(Context context, NodeScanner.NodeScannerCallbacks callbacks){
        NodeScanner result = null;
        if (Build.VERSION.SDK_INT < 21) {
            result = new NodeScanner18(context, callbacks);
        }
        else {
            result = new NodeScanner21(context, callbacks);
        }
        return(result);
    }

    abstract public void start();
    abstract public void finish();
}
