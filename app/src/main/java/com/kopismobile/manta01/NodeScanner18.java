package com.kopismobile.manta01;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by jderienzo on 8/26/2016.
 */
public class NodeScanner18 extends NodeScanner{
    private static final String TAG = NodeScanner18.class.getSimpleName();

    private static int   count = 0;

    private Context context = null;
    private Handler scanHandler = null;
    private NodeScannerCallbacks callbacks = null;
    private Set<String> foundDevices = null;
    private BluetoothAdapter bluetoothAdapter = null;
    private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
            if(NodeScanner18.this.foundDevices.add(device.toString())) {
                NodeScanner18.this.scanHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.i(TAG, "onLeScan(): " + device.toString());
                        NodeScanner18.this.bluetoothAdapter.stopLeScan(NodeScanner18.this.leScanCallback);
                        Log.v(TAG, "onLeScan(): scan stopped");
                        device.connectGatt(NodeScanner18.this.context, false, NodeScanner18.this.gattCallback);
                        // add the node to the unknowns
                        Cursor c = NodeScanner18.this.context.getContentResolver().query(
                                MantaContentProvider.MANTA_URI,
                                new String[]{MantaTable.ID},
                                MantaTable.UID + " = '" + device.toString() + "'",
                                null,
                                null
                        );
                        if((c == null) || (c.getCount() <= 0)) {
                            ContentValues cv = new ContentValues();
                            cv.put(MantaTable.UID, device.toString());
                            cv.put(MantaTable.IS_NEW, 1);
                            cv.put(MantaTable.GROUP_NAME, "<unknown>");
                            cv.put(MantaTable.NAME, "MantaNode-" + NodeScanner18.this.count);
                            cv.put(MantaTable.DISCOVERY_TIME, System.currentTimeMillis());
                            NodeScanner18.this.context.getContentResolver().insert(
                                    MantaContentProvider.MANTA_URI,
                                    cv
                            );
                            NodeScanner18.this.count++;
                        }
                        if(c != null){
                            c.close();
                        }

                        NodeScanner18.this.bluetoothAdapter.startLeScan(NodeScanner18.this.leScanCallback);
                        Log.v(TAG, "onLeScan(): scan started");
                        return;
                    }
                });
            }
            return;
        }

    };
    private BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(final BluetoothGatt gatt, final int status, final int newState) {
            NodeScanner18.this.scanHandler.post(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "onConnectionStateChange(): Status=" + status);
                    if(status != 0){
                        return;
                    }
                    switch (newState) {
                        case BluetoothProfile.STATE_CONNECTED:
                            Log.i(TAG, "gattCallback(): STATE_CONNECTED");
                            gatt.discoverServices();
                            gatt.disconnect();
                            break;
                        case BluetoothProfile.STATE_DISCONNECTED:
                            Log.i(TAG, "gattCallback():  STATE_DISCONNECTED");
                            break;
                        default:
                            Log.e(TAG, "gattCallback():  STATE_OTHER");
                    }
                }
            });
            return;
        }
        @Override
        public void onServicesDiscovered(final BluetoothGatt gatt, int status) {
            NodeScanner18.this.scanHandler.post(new Runnable() {
                @Override
                public void run() {
                    List<BluetoothGattService> services = gatt.getServices();
                    Log.i(TAG, "onServicesDiscovered(): " + services.toString());
                    if(services.size() > 0) {
                        if (services.get(1).getCharacteristics().size() > 0) {
                            gatt.readCharacteristic(services.get(1).getCharacteristics().get(0));
                        }
                    }
                    return;
                }
            });
        }
        @Override
        public void onCharacteristicRead(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic, int status) {
            NodeScanner18.this.scanHandler.post(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "onCharacteristicRead():" + characteristic.toString());
                    return;
                }
            });
        }
    };

    public NodeScanner18(Context context, NodeScanner.NodeScannerCallbacks callbacks){
        this.context = context;
        this.callbacks = callbacks;
        this.foundDevices = new HashSet<String>();
        final BluetoothManager btManager = (BluetoothManager) this.context.getSystemService(Context.BLUETOOTH_SERVICE);
        this.bluetoothAdapter = btManager.getAdapter();
    }

    public void start(){
        if(this.scanHandler == null) {
            HandlerThread ht = new HandlerThread("BLE ScannerThread");
            ht.start();
            this.scanHandler = new Handler(ht.getLooper());
        }
        this.bluetoothAdapter.startLeScan(this.leScanCallback);
        return;
    }

    public void finish(){
        Log.v(TAG, "finish()");
        this.bluetoothAdapter.stopLeScan(this.leScanCallback);
        if(this.scanHandler != null){
            this.scanHandler.postAtFrontOfQueue(new Runnable(){
                @Override
                public void run() {
                    Looper.myLooper().quit();
                    return;
                }
            });
            this.scanHandler = null;
        }
        return;
    }
}
