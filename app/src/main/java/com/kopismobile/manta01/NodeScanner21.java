package com.kopismobile.manta01;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Created by jderienzo on 8/23/2016.
 */
@TargetApi(21)
public class NodeScanner21 extends NodeScanner {
    private static final String TAG = NodeScanner21.class.getSimpleName();

    private Context context = null;
    private Handler scanHandler = null;
    private NodeScannerCallbacks callbacks = null;
    private Set<String> foundDevices = null;
    private BluetoothAdapter bluetoothAdapter = null;
    private BluetoothLeScanner leScanner;
    private BluetoothGatt gatt = null;
    private ScanSettings settings;
    private List<ScanFilter> filters;
    private ScanCallback scanCallback = new ScanCallback() {
        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
            return;
        }
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            if(NodeScanner21.this.foundDevices.add(result.getDevice().toString())) {
                Log.i(TAG, "onScanResult(): callbackType=" + String.valueOf(callbackType) + "result=" + result.toString());
                BluetoothDevice btDevice = result.getDevice();
                gatt = btDevice.connectGatt(NodeScanner21.this.context, false, gattCallback);
            }
            return;
        }
        @Override
        public void onScanFailed(int errorCode) {
            Log.e(TAG, "onScanFailed(): errorCode=" + errorCode);
            return;
        }
    };
    private BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.i(TAG, "onConnectionStateChange():status=" + status);
            switch (newState) {
                case BluetoothProfile.STATE_CONNECTED:
                    Log.i(TAG, "onConnectionStateChange(): STATE_CONNECTED");
                    gatt.discoverServices();
                    break;
                case BluetoothProfile.STATE_DISCONNECTED:
                    Log.i(TAG, "onConnectionStateChange(): STATE_DISCONNECTED");
                    break;
                default:
                    Log.i(TAG, "onConnectionStateChange(): unknown");
            }
            return;
        }
        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            List<BluetoothGattService> services = gatt.getServices();
            Log.i(TAG, "onServicesDiscovered():  services=" + services.toString());
            if((services.size() > 0) && (services.get(1).getCharacteristics().size()) > 0) {
                gatt.readCharacteristic(services.get(1).getCharacteristics().get(0));
            }
        }
        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.i(TAG, "onCharacteristicRead(): " + characteristic.toString());
            gatt.disconnect();
        }
    };

    public NodeScanner21(Context context, NodeScanner.NodeScannerCallbacks callbacks) {
        this.context = context;
        this.callbacks = callbacks;
        this.foundDevices = new HashSet<String>();
        final BluetoothManager btManager = (BluetoothManager) this.context.getSystemService(Context.BLUETOOTH_SERVICE);
        this.bluetoothAdapter = btManager.getAdapter();
        this.leScanner = this.bluetoothAdapter.getBluetoothLeScanner();
        this.settings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).build();
        this.filters = new ArrayList<ScanFilter>();
    }

    @Override
    public void start() {
        if(this.scanHandler == null) {
            HandlerThread ht = new HandlerThread("BLE ScannerThread");
            ht.start();
            this.scanHandler = new Handler(ht.getLooper());
        }
        final BluetoothManager btManager = (BluetoothManager) this.context.getSystemService(Context.BLUETOOTH_SERVICE);
        this.bluetoothAdapter = btManager.getAdapter();
        this.leScanner.startScan(filters, settings, this.scanCallback);
        return;
    }

    @Override
    public void finish() {
        Log.v(TAG, "finish()");
        this.leScanner.stopScan(this.scanCallback);
        if(this.scanHandler != null){
            this.scanHandler.postAtFrontOfQueue(new Runnable(){
                @Override
                public void run() {
                    Looper.myLooper().quit();
                    return;
                }
            });
            this.scanHandler = null;
        }
        return;
    }
}
