package com.kopismobile.manta01;

import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

/**
 * Created by jderienzo on 8/31/2016.
 */
public class ToGroupDialog implements DialogInterface.OnCancelListener, DialogInterface.OnClickListener, AdapterView.OnItemSelectedListener {
    private static final String TAG = ToGroupDialog.class.getSimpleName();

//    private final Dialog dialog;
    private final AppCompatActivity activity;

    private Dialog dialog;
    private Spinner spinner = null;
    private ToGroupCallback callback = null;
    private String fromGroup = null;
    private String result = null;
    private AlertDialog.Builder dialogBuilder = null;

    public interface ToGroupCallback {
        void onGroupName(String name);
    }

    public ToGroupDialog(AppCompatActivity activity, final String fromGroup, ToGroupCallback callback){
        this.activity = activity;
        this.fromGroup = fromGroup;
        /*
        this.callback = callback;
        this.dialog = new Dialog(activity, R.style.manta_dialog);
        this.dialog.setTitle("Destination Group");
        this.dialog.setContentView(R.layout.to_group_dialog);
        this.dialog.setOnCancelListener(this);
        */

        LayoutInflater inflater = LayoutInflater.from(this.activity);
        View dialog_layout = inflater.inflate(R.layout.to_group_dialog,(ViewGroup) this.activity.findViewById(R.id.toGroupLayout));
        this.dialogBuilder = new AlertDialog.Builder(this.activity);
        dialogBuilder.setOnCancelListener(this);
        dialogBuilder.setPositiveButton("Move", this);
        dialogBuilder.setNegativeButton("Cancel", this);
        dialogBuilder.setView(dialog_layout);
        dialogBuilder.setTitle("Destination Group:");
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        Log.v(TAG, "onClick(): i=" + i + ", dialogInterface=" + dialogInterface);
        if(i == DialogInterface.BUTTON_POSITIVE){

        }
        else{

        }
        if(this.callback != null){
            this.callback.onGroupName(this.result);
        }
        this.dialog.dismiss();
        return;
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        Log.v(TAG, "onClick(): dialogInterface=" + dialogInterface);
        if(this.callback != null){
            this.callback.onGroupName(this.result);
        }
        this.dialog.dismiss();
        return;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Log.v(TAG, "onItemSelected(): adapterView=" + adapterView + ", view=" + view);
        return;
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        Log.v(TAG, "onItemSelected(): adapterView=" + adapterView);
        return;
    }

    public void show(){
        (new Thread(new Runnable() {
            @Override
            public void run() {
                String where = null;
                if(fromGroup != null){
                    where = GroupTable.NAME + " <> '" + fromGroup + "'";
                }
                Cursor cursor = ToGroupDialog.this.activity.getContentResolver().query(
                        MantaContentProvider.GROUP_URI,
                        new String[]{GroupTable.NAME},
                        where,
                        null,
                        null
                );
                if(cursor != null){
                    if((cursor.getCount() > 0) && (cursor.moveToFirst())){
//                        final ArrayAdapter<String> aa = new ArrayAdapter<String>(ToGroupDialog.this.activity, android.R.layout.simple_spinner_item);
                        final ArrayAdapter<String> aa = new ArrayAdapter<String>(ToGroupDialog.this.activity, R.layout.spinner_text_item);
                        while(!cursor.isAfterLast()){
                            aa.add(cursor.getString(cursor.getColumnIndex(GroupTable.NAME)));
                            cursor.moveToNext();
                        }
                        ToGroupDialog.this.activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ToGroupDialog.this.dialog = ToGroupDialog.this.dialogBuilder.show();
                                ToGroupDialog.this.spinner = (Spinner) ToGroupDialog.this.dialog.findViewById(R.id.toGroupSpinner);
                                ToGroupDialog.this.spinner.setAdapter(aa);
                                ToGroupDialog.this.spinner.setOnItemSelectedListener(ToGroupDialog.this);
//                                ((Spinner)(ToGroupDialog.this.dialog.findViewById(R.id.toGroupSpinner))).setAdapter(aa);
                                return;
                            }
                        });
                    }
                    cursor.close();
                }
                return;
            }
        }, "ToDialogThread")).start();
        return;
    }

    public void xxshow(){
        (new Thread(new Runnable() {
            @Override
            public void run() {
                String where = null;
                if(fromGroup != null){
                    where = GroupTable.NAME + " <> '" + fromGroup + "'";
                }
                Cursor cursor = ToGroupDialog.this.activity.getContentResolver().query(
                        MantaContentProvider.GROUP_URI,
                        new String[]{GroupTable.NAME},
                        where,
                        null,
                        null
                );
                if(cursor != null){
                    if((cursor.getCount() > 0) && (cursor.moveToFirst())){
                        final ArrayAdapter<String> aa = new ArrayAdapter<String>(ToGroupDialog.this.activity, R.layout.spinner_text_item);
                        while(!cursor.isAfterLast()){
                            aa.add(cursor.getString(cursor.getColumnIndex(GroupTable.NAME)));
                            cursor.moveToNext();
                        }
                        ToGroupDialog.this.activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                                ((Spinner)(ToGroupDialog.this.dialog.findViewById(R.id.toGroupSpinner))).setAdapter(aa);
//                                ToGroupDialog.this.dialog.show();
                                return;
                            }
                        });
                    }
                    cursor.close();
                }
                return;
            }
        }, "ToDialogThread")).start();
        return;
    }
}
